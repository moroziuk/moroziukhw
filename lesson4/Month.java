package com.company.HomeWork.lesson4;

public class Month {
/**
 * 1  -> 31 | "31"
 * 2  -> 28 | "30"
 * 3  -> 31 | "31"
 * 4  -> 30 | "30"
 * 5  -> 31 | "31"
 * 6  -> 30 | "30"
 * 7  -> 31 | "31"
 * 8  -> 31 | "30"
 * 9  -> 30 | "31"
 * 10 -> 31 | "30"
 * 11 -> 30 | "31"
 * 12 -> 31 | "30" */
    public static int nDays(int month) {
        return 30 + month % 2 - month / 2 - month / 2 + month / 3 + month / 3 + month / 4 + month / 4 + month / 8 - month / 9 - month / 9 - month / 9
                - month / 9 + month / 10 + month / 10 + month / 10 + month / 10 - month / 11 - month / 11;
    }

    public static void main(String[] args) {
        System.out.println(nDays(1));  // 31
        System.out.println(nDays(2));  // 28
        System.out.println(nDays(3));
        System.out.println(nDays(4));// 31
        System.out.println(nDays(5));
        System.out.println(nDays(6));
        System.out.println(nDays(7));
        System.out.println(nDays(8));
        System.out.println(nDays(9));
        System.out.println(nDays(10));
        System.out.println(nDays(11));
        System.out.println(nDays(12)); // 31
    }

}
