package com.company.HomeWork.lesson4;

public class MultiArrays {
    static int calculateElement(int x, int y) {
         if (x == 0 && y == 0) {
             return 1;
         }
         else return (x+1)+(y*20);
         }

    static void fill(int[][] data) {
        int W = data[0].length;
        int H = data.length;

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                data[y][x] = calculateElement(x, y);
            }
        }
    }

    static int calculateX(int i) {
        if ((i)/20%2==0) {
            return i%20;
        }
else return 19-i%20;
    }

    static int calculateY(int i) {
         if (i<20) {
             return 0;}
         else if (i<40) {
             return 1;}
         else if (i<60) {
             return 2;}
         else if (i<80) {
             return 3;}
         else if (i<100) {
             return 4;}
         else if (i<120) {
             return 5;}
         else if (i<140) {
             return 6;}
         else if (i<160) {
             return 7;}
         else if (i<180) {
             return 8;}
        else return 9;
        }

    static boolean isNewline(int i) {
        return (i+1)%20==0;
    }

    public static void print(int[][] data) {
        int W = data[0].length;
        int H = data.length;
        for (int i = 0; i < W * H ; i++) {
            int x = calculateX(i);
            int y = calculateY(i);
            int el = data[y][x];
            System.out.printf("%3d ", el);
            if (isNewline(i)) System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] xss = new int[10][20];

        fill(xss);

        print(xss);
    }
}

